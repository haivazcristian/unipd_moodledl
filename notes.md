# UniPD Scraper
## Idea
Our final objective is to have a light, performant, modular and self-serving scraper for the UniPD network 
allowing students and users to easily download the contens of the courses they desire, safely and quickly


## TODO:
- [x] Install MS and setup basic environment
- [x] Setup login system
- [x] Study tags inside page
- [x] Obtain basic results such as all links and types
- [x] Implement basic filtering and downloads
- [x] Implement basic GUI
- [x] Try to package
- [x] Add suppport for more departments and more possibilities


## PySimpleGUI
Shown itself to be decent enough, easy and complete

Build a layout via lists and simple structures with Buttons and text

Then an Event Loop handles events

The `perform_long_operation` functions is just a daemon thread, with a added `window.write_event_value` at the end. 
Threads do share variables, but `values` is updated constantly by PySimpleGUI, so you can't write to it reliably.

What you can do is return a dictionary as the thread ends containg all the info needed and thenadd that.
When the thread ends it calls the `END_EVENT` and stores the return in `values[END_EVENT]`.

Values shpould really not be used for internal data, for that we created a data dictionary adn we'll use that, we'll need to clea up some code but it will be massively easier to use in the future.

## PyInstaller
Useful on windows, need to test if kaltura downlload has any issues

command: 

    pyinstaller --onefile --clean -w -n unipd_moodledl_$version_num unipd_moodledl.py

Compilation is succesful
It does not incorporate ffmpeg as of now, might become an issue later


## Moodle-Downloader-2

Works well
very well
issues is:
 - unfriendly UX thorugh the CLI
 - need to extract a token from your browser, uncomfortable
 - kinda clunky

token extraction is easily automated with our mechbrowser solution

issues are linked to how moodle is always different, so it's hard to downlaod everything even with good software
examples are mediaspace folders of videos, those are just not supported

do other courses have wildly different needs?

most stuff can be selected or not with checkboxes, but we need to be sure what they actually are


We can reach most content, including videos, by:
1. Login via requests
2. Open special page to extract associated Moodle Token
3. Save all reamiaining cookies to downloader Jar
4. Generate the needed tokes for the downloader from the aforementioned one
5. download content from a course with the `"download_also_with_cookie": true` value 

thr Log module is NOT for logging, but fancy printing. it calls print under the hood

## Extra Features
 - Option for download location
 - right click on course to get moodle link 
 - Signal when a course download failed (eg, no folder was created)
 - Way to keep download size in check
 - Button to open folder where files are downloaded
 - advanced panel for downloads to filter for file extension
 - use fake download to compute real download size (?)

### - Option for download location
This requires either copying the guts of `run_main` and decouplign downloads, config and logs from the same shared path or accepting they will all be moved there

Decouplign would also have to keep track of any possible hidden path check that would be violated, such as Cookies being invoked by the Moodle Service in the path, so they need to be in the same path. 

Downloads seem to be all handeled by DownloadService, generating the URLtargets that containg their own paths, so it might not have other dependencies

relevant issue https://github.com/C0D3D3V/Moodle-Downloader-2/issues/157

#### - use fake download to compute real download size (?)
They say this coould be done somewhat biut the big limit is videos, whcih are just dropped to yt-dlp and that works mostly by itself, we would need to check it's documentation and see how we can read the sie esitmates and then sume them up.

#### - advanced panel for downloads to filter for file extension
The file extension filter is only vaild for non-cookies downloads, for the others there is no filter, we could rerun the entire folder and remove files maybe? feels mediocre and dangerous, especially in the hands of non-expert users

Also the balcklist method only checks domains and does not support a regex check. it's done in ,`is_filtered_external_domain` in `url_target`, we might want to add our check or make an issue or even a pull request 

#### - right click on course to get moodle link 
the way simpleGUI is built this is not happening easily

## Releases

Adding files to releases is kind of a pain

using cUrl from WSL we can load the files as needed, but it's a suboptimal solution

We could implement a CI/CD environment that is triggered on main commits, generates the EXE and loads it to the enviroment

or we could try and move to GitHub, where there are probably similar issues, idk


## Bugs
 - Some moodle urls are wrong, link to specific course and not department
 - need to check with someone that has an account in the departments to see how to fix it    

## Download options Notes

Submissions are files that you or a teacher have uploaded to your assignments. Moodle does not provide an interface for downloading information from all submissions to a course at once.
Therefore, it may be slow to monitor changes to submissions.

In Moodle courses, descriptions can be added to all kinds of resources, such as files, tasks, assignments or simply free text. These descriptions are usually unnecessary to download because you have already read the information or know it from context. However, there are situations where it might be interesting to download these descriptions. The descriptions are created as Markdown files and can be deleted as desired.
Creating the description files does not take extra time, but they can be annoying if they only contain unnecessary information.


In the descriptions of files, sections, assignments or courses the teacher can add links to webpages, files or videos. That links can point to a internal page on moodle or to an external webpage.

In the database module of Moodle data can be stored structured with information. Often it is also possible for students to upload data there.  Because the implementation of the downloader has not yet been optimized at this point, it is optional to download the databases. Currently only files are downloaded, thumbails are ignored.

In forums, students and teachers can discuss and exchange information together.

Quizzes are tests that a student must complete in a course and are graded on. Only quizzes that are in progress or have been completed will be downloaded.


Lessons are a kind of self-teaching with pages of information and other pages with questions to answer. A student can be graded on their answers after completing a lesson. Currently, only lessons without the answers are downloaded. The answers are potentially also available for download, but this has not been implemented.

Workshops function according to the peer review process. Students can make submissions and have to assess submissions of other students. 


In Moodle courses the teacher can also link to external files. This can be audio, video, text or anything else. In particular, the teacher can link to Youtube videos.
To download videos correctly you have to install ffmpeg. 
These files can increase the download volume considerably.
If you want to filter the external links by their domain, you can manually set a whitelist and a blacklist (https://github.com/C0D3D3V/Moodle-Downloader-2/wiki/Download-(external)-linked-files for more details).
Please note that the size of the external files is determined during the download, so the total size changes during the download.