import PySimpleGUI as sg
from PySimpleGUI import Text, Button, Window
import mechanicalsoup
from http.cookiejar import MozillaCookieJar
from requests.exceptions import InvalidSchema
from moodle_dl.config_service.config_helper import ConfigHelper
from urllib.parse import urlparse
from moodle_dl.moodle_connector.moodle_service import MoodleService
from moodle_dl.moodle_connector import sso_token_receiver
from moodle_dl.moodle_connector.first_contact_handler import FirstContactHandler
from moodle_dl.moodle_connector.request_helper import RequestHelper
from moodle_dl.main import run_main

from pathlib import Path
import os
import logging
import argparse
import webbrowser
import textwrap
import base64


version = "v0.0.3.1"

cookie_jar_path = "Cookies.txt"
storage_path = "."
config_helper = ConfigHelper(storage_path)
moodle_list = ["STEM - Macroarea", 'DAFNAE - Agronomy, Food, Natural resources, Animals and Environment', 'DBC - Cultural Heritage: Archaeology and History of art, Cinema and Music', 'DiBIO - Biology', 'BCA - Comparative biomedicine and Food science', 'DPCD - Private law and critique of law', 'DiPIC - Public, international and community law', 'FISPPA - Philosophy, Sociology, Education and Applied psychology', "DFA - Physics and Astronomy 'Galileo Galilei'", 'GEO - Geosciences', 'ICEA - Civil, environmental and architectural engineering', 'DEI - Information engineering', 'DII - Industrial engineering', 'DM - Mathematics', 'DIMED - Medicine', 'MAPS - Animal medicine, production and health', 'DMM - Molecular Medicine',
               'DPSS - Developmental psychology and socialisation', 'DPG - General psychology', "SDB - Women's and children's health", 'DSB - Biomedical sciences', 'DCTV - Cardiac, thoracic, vascular sciences and public health', 'DiSC - Chemical sciences', 'DiSCOG - Surgery, oncology and gastroenterology', 'DSF - Pharmaceutical and pharmacological sciences', "DSEA - Economics and management 'Marco Fanno'", 'SPGI - Political Science, Law and International Studies', 'STAT - Statistical sciences', 'DISSGeA - Historical and geographic sciences and the ancient world', 'DISLL - Linguistic and literary studies', 'DTG - Management and engineering', 'TESAF - Land, environment, agriculture and forestry']

moodle_urls = ["https://stem.elearning.unipd.it/", 'https://elearning.unipd.it/scuolaamv/', 'https://elearning.unipd.it/scienzeumane/mod/page/', 'https://elearning.unipd.it/biologia/', 'https://elearning.unipd.it/scuolaamv/', 'https://elearning.unipd.it/giurisprudenza/', 'https://elearning.unipd.it/giurisprudenza/', 'https://elearning.unipd.it/scienzeumane/mod/page/', 'https://elearning.unipd.it/dfa/', 'https://elearning.unipd.it/geoscienze/', 'https://elearning.unipd.it/dicea/', 'https://elearning.dei.unipd.it/', 'https://elearning.unipd.it/dii/', 'https://elearning.unipd.it/math/', 'https://elearning.unipd.it/dimed/', 'https://elearning.unipd.it/scuolaamv/',
               'https://elearning.unipd.it/medicinamolecolare/', 'https://elearning.unipd.it/scuolapsicologia/', 'https://elearning.unipd.it/scuolapsicologia/', 'https://elearning.unipd.it/sdb/', 'https://elearning.unipd.it/dsb/', 'https://elearning.unipd.it/dctv/', 'https://elearning.unipd.it/chimica/', 'https://elearning.unipd.it/discog/', 'https://elearning.unipd.it/dsf/', 'https://elearning.unipd.it/economia/', 'https://elearning.unipd.it/spgi/', 'https://elearning.unipd.it/stat/', 'https://elearning.unipd.it/scienzeumane/mod/page/', 'https://elearning.unipd.it/scienzeumane/mod/page/', 'https://elearning.unipd.it/dtg/', 'https://elearning.unipd.it/scuolaamv/']


courses = []
courses_names = []
data = {}


def generate_UI():
    sg.theme('BluePurple')

    email_col = [
        [sg.Text("Email")],
        [sg.Input(key='email', size=36, expand_x=True)]]

    pwd_col = [
        [sg.Text("Password")],
        [sg.Input(key='password', size=14, password_char="*")]]

    login_frame = sg.Frame("Login", [
        [sg.Col(email_col), sg.Col(pwd_col),
            sg.Button("Login", size=(8, 2), key="login")],
        [sg.Text("Department")],
        [sg.Combo(values=moodle_list, key="moodle")]])

    checkboxes = [
        sg.Col([
            [sg.Checkbox("Basic content", key="",
                         tooltip="All normal files, such as PDFs, Excell, Word and so on. This cannot be disabled.", default=True, disabled=True)],
            [sg.Checkbox("Videos", key="get videos",
                         tooltip="WARNING: might be >10GB\nKaltura videos and others linked diretly in the Moodle page. Does not work for links hidden in text.")],
            [sg.Checkbox("Assignment submissions", key="get submissions",
                         tooltip="All submitted homeworks and similar assignments")],
            [sg.Checkbox("Moodle text", key="get descriptions",
                         tooltip="Every description and part of text in the page. Probably mostly redundant")],
            [sg.Checkbox("From links in text", key="get links",
                         tooltip="Search for any link in the text and download all files")],
            [sg.Checkbox("Forum", key="get forums", tooltip="The entire forum")]]),
        sg.Col([
            [sg.Checkbox("Database", key="get databases", tooltip="IDK")],
            [sg.Checkbox("Quizzes", key="get quizzes",
                         tooltip="I'm not sure if I've ever seen one")],
            [sg.Checkbox("Interactive lessons", key="get lessons",
                         tooltip="I'm not sure if I've ever seen one")],
            [sg.Checkbox("Workshop", key="get workshops", tooltip="Peer reviewed homeworks")]])]

    options_and_download_col = [checkboxes, [sg.Button("Download")]]

    download_frame = sg.Frame("Download", [[sg.Col(options_and_download_col)]])

    col_sx = sg.Col([[login_frame], [download_frame]])

    col_dx = sg.Frame("Courses", [[sg.Col([[sg.Listbox(courses_names, size=(
        50, 19), key="course_list", select_mode=sg.LISTBOX_SELECT_MODE_MULTIPLE, enable_events=True)], [sg.Text("Selected courses: "), sg.Text(key="-N_COURSES-")]])]])

    output_frame = sg.Frame("Log", [[sg.Multiline(size=(132, 14), reroute_stdout=True,
                            reroute_stderr=True, echo_stdout_stderr=True, auto_refresh=True, autoscroll=True)]])

    layout = [[sg.Menu([['More', ['Debug', "About..."]]])],
              sg.vtop([col_sx, col_dx]), [output_frame]]


    return sg.Window('UniPD MoodleDL ' + version, layout, finalize=True)


def login(values, data):
    logging.info("Login thread started")

    return_value = {}
    return_value["login_response"] = False
    return_value["courses"] = None
    moodle_url = data["moodle_url"]

    browser = mechanicalsoup.StatefulBrowser()  # generate automated browser
    if data["debug"]:
        browser.set_debug(True)  # activate debug
        browser.set_verbose(2)  # print all visited links
    response = browser.open(
        moodle_url + "auth/shibboleth/index.php")  # reach login page
    if response.status_code != 200:
        return_value["login_response"] = "ERROR REACHING LOGIN PAGE"
        logging.error(return_value["login_response"])
        logging.error("URL: " + moodle_url + "auth/shibboleth/index.php")
        logging.error("Response: " + str(response.content))
        return return_value
    logging.debug("Reached Login page")

    # Needed as the non-JS browser requires an extra input to work
    browser.select_form('form')
    browser.submit_selected()

    # Select and compile form, then submit

    browser.select_form('form')
    if "@" not in values["email"]:
        print("Email does not contain @. adding '@studenti.unipd.it'")
        values["email"] += "@studenti.unipd.it"
    browser["j_username_js"] = values["email"]
    browser["j_username"] = values["email"]
    browser["j_password"] = values["password"]
    response = browser.submit_selected()
    # Again click due to being a non-JS browser
    browser.select_form('form')
    response = browser.submit_selected()
    logging.debug("Inserted Login info")

    # need to understand how to check if the login was successful
    token_address = None
    try:
        response = browser.open(
            moodle_url + "admin/tool/mobile/launch.php?service=moodle_mobile_app&passport=12345")
    except InvalidSchema as err:
        token_address = str(err)
        # print the token to screen
    except:
        logging.exception("Error during token extraction")
    if token_address is None:
        return_value["login_response"] = "MOODLE LOGIN FAILED"
        logging.error(return_value["login_response"])
        logging.error("Token address is None, probably wrong credentials")
        return return_value
    browser.open(moodle_url)
    logging.info("Token address read succesfully")

    # save etra cookies to file
    login_cookie_jar = browser.get_cookiejar()
    cookies_jar = MozillaCookieJar(cookie_jar_path)
    if os.path.exists(cookie_jar_path):
        cookies_jar.load(ignore_discard=True, ignore_expires=True)
    for cookie in login_cookie_jar:
        cookie.expires = 2147483647
        cookies_jar.set_cookie(cookie)

    cookies_jar.save(ignore_discard=True, ignore_expires=True)
    logging.info("Cookies generated succesfully")

    moodle_service = MoodleService(config_helper, storage_path)

    moodle_uri = urlparse(moodle_url)
    moodle_domain, moodle_path = moodle_service._split_moodle_uri(moodle_uri)
    config_helper.set_property('moodle_domain', moodle_domain)
    config_helper.set_property('moodle_path', moodle_path)

    moodle_token, moodle_privatetoken = sso_token_receiver.extract_token(
        token_address)
    if moodle_token is None:
        return_value["login_response"] = 'MOODLE TOKEN: Invalid URL!'
        logging.error(return_value["login_response"])
        logging.error("Token address: " + token_address)
        logging.error("Moodle URI: " + moodle_uri)
        return

    # Saves the created token and the successful Moodle parameters.
    config_helper.set_property('token', moodle_token)
    if moodle_privatetoken is not None:
        config_helper.set_property('privatetoken', moodle_privatetoken)

    request_helper = RequestHelper(
        moodle_domain, moodle_path, moodle_token, False, use_http=False)
    first_contact_handler = FirstContactHandler(request_helper)
    userid, moodle_version = first_contact_handler.fetch_userid_and_version()
    logging.debug("Moodle Version:")
    logging.debug(moodle_version)
    courses = first_contact_handler.fetch_courses(userid)

    return_value["courses"] = courses
    return_value["login_response"] = True
    logging.info("Login succesful")
    return return_value


def generate_config(window):
    if not window["get videos"].get():
        config_helper.set_property("download_domains_blacklist", [
            "youtube.com",
            "twitch.tv",
            "kaltura.com",
            "mediaspace.unipd.it"
        ])
        config_helper.set_property("exclude_file_extensions", [
            "mp4",
            "mkv",
            "flv",
            "mov",
            "avi",
            "wmv",
            "webm"
        ])
    elif window["get videos"].get():
        config_helper.remove_property("exclude_file_extensions")
        config_helper.remove_property("download_domains_blacklist")

    config_helper.set_property(
        'download_submissions', window["get submissions"].get())
    config_helper.set_property(
        'download_descriptions', window["get descriptions"].get())
    config_helper.set_property(
        'download_links_in_descriptions', window["get links"].get())
    config_helper.set_property(
        'download_databases', window["get databases"].get())
    config_helper.set_property(
        'download_quizzes', window["get quizzes"].get())
    config_helper.set_property(
        'download_lessons', window["get lessons"].get())
    config_helper.set_property(
        'download_workshops', window["get workshops"].get())
    config_helper.set_property(
        "download_also_with_cookie", True)


def info_popup():

    body = """This is a simple Program to download content from the various UniPD Departments

The official website of this program is https://gitlab.com/Udinanon/unipd_moodledl, come visit!
If you have found any bugs, issues or have any proposals, you can come to out GitLab an open up an issue, contact us on Telegram at @udinanon or send an email at udinanon@tutamail.com. 
Don't forget to send the .log files along!

This program is version """+version
    body_wrapped = ''
    # break into segments that will each be wrapped
    msg_list = body.split('\n')
    body_wrapped = '\n'.join(
        [textwrap.fill(msg, 60) for msg in msg_list])
    layout = [[Text(body_wrapped, auto_size_text=True)], [Button("OK", focus=True, bind_return_key=True, size=(len(" OK "), 1)), Button(
        "GitLab", focus=True, size=(len("GitLab"), 1)), Button("Telegram", focus=True, size=(len("Telegram"), 1)), Button("Email", focus=True, size=(len("Email"), 1))]]
    popup = Window("About", layout, auto_size_text=True,
                   keep_on_top=True, finalize=True)
    button, values = popup.read()
    popup.close()
    del popup
    return button


def file_cleanup():
    if os.path.exists(storage_path + "/" + "moodle_state.db"):
        os.remove(storage_path + "/" + "moodle_state.db")
        logging.debug("Removed preexisting moodle_state.db")
    if os.path.exists(storage_path + "/" + "running.lock"):
        os.remove(storage_path + "/" + "running.lock")
        logging.debug("Removed preexisting running.lock")
    if os.path.exists(storage_path + "/" + "Cookies.txt"):
        os.remove(storage_path + "/" + "Cookies.txt")
        logging.debug("Removed preexisting Cookies.txt")
    if os.path.exists(storage_path + "/" + "config.json"):
        os.remove(storage_path + "/" + "config.json")
        logging.debug("Removed preexisting config.json")


parser = argparse.ArgumentParser(description='Moodle Download tool for UniPD')
parser.add_argument("-d", "--debug", help="increase output verbosity",
                    action="store_true", dest="debug")

args = parser.parse_args()
data["debug"] = args.debug
if args.debug:
    logging.basicConfig(format='%(asctime)s  %(levelname)s  {%(module)s}  %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S', level=logging.DEBUG, filename="moodledl.log")
else:
    logging.basicConfig(format='%(asctime)s  %(levelname)s  {%(module)s}  %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S', level=logging.INFO, filename="moodledl.log")


if __name__ == "__main__":
    logging.debug("Main started")
    file_cleanup()
    
    window = generate_UI()  # generate UI component
    logging.debug("UI Generated")
    while True:  # Event Loop
        event, values = window.read()

        if event == sg.WIN_CLOSED or event == 'Exit':
            logging.info("Exit signal received")
            break

        if event == 'login':
            logging.info("Login command")
            moodle_url = None
            if values["moodle"] == "":
                print("Please select a valid department from the list")
            else:
                try:
                    moodle_url = moodle_urls[moodle_list.index(
                        values["moodle"])]
                except ValueError:
                    print("Please select a valid department from the list")
            if values["email"] and values["password"] and moodle_url is not None:
                logging.debug("All fields non empty")
                data["moodle_url"] = moodle_url
                logging.debug("Moodle URL used:")
                logging.debug(moodle_url)
                window.refresh()

                print("Logging in, please wait...")
                window.perform_long_operation(
                    lambda: login(values, data), "LOGIN ENDED")

        if event == "LOGIN ENDED":
            data.update(values["LOGIN ENDED"])
            if data["login_response"] == True:
                if data["courses"] is None:
                    print(
                        "No courses found. Are you sure you have courses in this department?")
                else:
                    logging.info("Login values are OK")
                    for course in data["courses"]:
                        courses_names.append(course.fullname)
                    print("Found ", len(courses_names), " valid courses")
                    window["course_list"].update(courses_names)
                    logging.debug("Courses found:")
                    logging.debug(courses_names)
                    window.refresh()
            else:
                logging.error(data["login_response"])
                logging.error("failed to retrieve courses!")
                print("failed to retrieve courses!")

        if event == "Download":
            logging.info("Download event")
            window.perform_long_operation(
                lambda: generate_config(window), "Config ready")
            logging.info("Config ready")

        if event == "Config ready":
            selected_courses = window["course_list"].get()
            course_ids = []
            for course in data["courses"]:
                if course.fullname in selected_courses:
                    course_ids.append(course.id)
            if course_ids == []:
                print("No selected courses, please select a course")
            else:
                window.write_event_value("Valid courses selected", None)

        if event == "Valid courses selected":
            config_helper.set_property('download_course_ids', course_ids)
            logging.debug(os.path.realpath(storage_path))
            window.perform_long_operation(lambda: run_main(
                storage_path,
                verbose=data["debug"],
                skip_cert_verify=False,
                ignore_ytdl_errors=False,
                without_downloading_files=False,
                log_responses=False
            ), "Download finished")

        if event == "Download finished":
            logging.info("Download finished")
            window.ding(display_number=0)
            print("DONE")
            if os.path.exists(storage_path + "/" + "moodle_state.db"):
                os.remove(storage_path + "/" + "moodle_state.db")
            print("REMOVED MOODLE DB")
            logging.info("Removed Moodle DB")

        if event == "course_list":
            window["-N_COURSES-"].update(len(values["course_list"]))

        if event == "About...":
            button_press = info_popup()

            if button_press == "GitLab":
                webbrowser.open("https://gitlab.com/Udinanon/unipd_moodledl")

            if button_press == "Telegram":
                webbrowser.open("https://t.me/Udinanon")

            if button_press == "Email":
                webbrowser.open("mailto:udinanon@tutamail.com")

        if event == "Debug":
            print("Debug mode on")
            data["debug"] = True
            logging.debug("Debug mode on")

    window.close()
