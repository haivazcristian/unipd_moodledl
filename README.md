# UniPD MoodleDL

[![Latest Release](https://gitlab.com/Udinanon/unipd_moodledl/-/badges/release.svg)](https://gitlab.com/Udinanon/unipd_moodledl/-/releases)

A GUI and support script to help UniPD users in downloading everything they want or need from the UniPD Moodle platform. 
Just login, select the course and download away!

## Installing

If you're on Windows, just head to [the latest release](https://gitlab.com/Udinanon/unipd_moodledl/-/releases/permalink/latest)

On Linux or MacOS, I hope you're ready to use git and python:

download or clone the repository 
```bash 
mkdir unipd_moodledl
cd unipd_moodledl
git clone https://gitlab.com/Udinanon/unipd_moodledl.git .
```

install all required packages for python
```bash
python -m venv moodledl_env
source moodledl_env/bin/activate
pip instal -r reqs.txt
```

then run!
```bash
python unipd_moodledl.py
```


## How it works

This is just a front end script for the much smarter [Moodle-Downloader-2](https://github.com/C0D3D3V/Moodle-Downloader-2)

Foir the rest, it's built in Python using: 
 - [PySimpleGUI](https://www.pysimplegui.org/) for the interface
 - [PyInstaller](https://pyinstaller.org/) to build the EXE packages
 - [MechanicalSoup](https://mechanicalsoup.readthedocs.io/en/stable/) to automate the login process
 - [Tabler Icons](https://tabler-icons.io/) for the free icon

## Contributing

I have limied experience working on public projects, so bear with me a bit.

The `notes.md` file contains most of my notes for decvelopment, so it might be of interest.

Check out the other branches, `main` will be used only for releases going forward, check out `dev`

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## Contacting

If you find bugs or issues or ave any ideas or want to say thanks you can find me via email at udinanon@tutamail.com or via Telegram at @udinanon

## License

We'll using GPL v3 to ensure compatibility with any libraries, if there are any issues please tell me 